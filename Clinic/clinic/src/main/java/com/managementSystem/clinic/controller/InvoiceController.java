package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.InvoiceRequestDTO;
import com.managementSystem.clinic.DTOs.PatientInvoiceDTO;
import com.managementSystem.clinic.model.Invoice;
import com.managementSystem.clinic.repository.InvoiceRepository;
import com.managementSystem.clinic.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/L2/")
public class InvoiceController {


//    @Autowired
    private final InvoiceService invoiceService;

    @PutMapping("makeInvoiceWithServices")
    public ResponseEntity<String>  makeInvoiceWithServices(@RequestBody InvoiceRequestDTO invoiceRequestDTO){
        try {
            invoiceService.makeInvoiceWithServices(invoiceRequestDTO);
            return ResponseEntity.ok("Invoice has been Created");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }



    @GetMapping("getAllInvoicesBy/{patientId}")
    public ResponseEntity<List<PatientInvoiceDTO>>  getAllInvoicesByID(@PathVariable int patientId){
        try {
            return ResponseEntity.ok(invoiceService.getAllInvoicesByID(patientId));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


}
