package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.*;
import com.managementSystem.clinic.model.Medicament;
import com.managementSystem.clinic.model.Patient;
import com.managementSystem.clinic.model.Recipe;
import com.managementSystem.clinic.repository.MedicamentRepository;
import com.managementSystem.clinic.repository.PatientRepository;
import com.managementSystem.clinic.repository.RecipeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

@Service
@RequiredArgsConstructor
public class RecipeService {



    private final RecipeRepository recipeRepository;

    private final PatientRepository patientRepository;

    private final MedicamentRepository medicamentRepository;


    //Not in Use
    public void addRecipeToPatient(int patientId, Recipe recipe) {
        Patient patient = patientRepository.getReferenceById(patientId);
        patient.getRecipes().add(recipe);
        recipe.setPatient(patient);

        recipeRepository.save(recipe);
        patientRepository.save(patient);

    }

    public List<PatientRecipesResponsesDTO> getAllPatientRecipesById(int patientId) {
        Optional<Patient> patient = patientRepository.findById(patientId);
        List<PatientRecipesResponsesDTO> patientRecipesDTOS = new ArrayList<>();

        patient.get().getRecipes().forEach( recipe -> {
            PatientRecipesResponsesDTO tempObject = new PatientRecipesResponsesDTO();
            tempObject.setMedicamentList(new ArrayList<>());
            tempObject.setRecipeId(recipe.getRecipeId());
            tempObject.setPatientName(recipe.getPatientName());
            tempObject.setPatientAddress(recipe.getPatientAddress());
            tempObject.setDateOfIssue(recipe.getDateOfIssue());
            tempObject.setRecipeNumber(recipe.getRecipeNumber());
            tempObject.setE_card(recipe.getE_card());

            recipe.getMedicamentList().forEach(
                    medicament -> {
                        MedicamentDTO medicamentDTO = new MedicamentDTO();
                        medicamentDTO.setMedicamentId(medicament.getMedicamentId());
                        medicamentDTO.setDose(medicament.getDose());
                        medicamentDTO.setName(medicament.getName());
                        medicamentDTO.setProducer(medicament.getProducer());
                        tempObject.getMedicamentList().add(medicamentDTO);
                    }
            );

            patientRecipesDTOS.add(tempObject);
        });
        return patientRecipesDTOS;
    }


    public void makeRecipeWithMedicament(RecipeRequestDTO recipeOrder) {
        Optional<Patient> patient = patientRepository.findById(recipeOrder.getPatientID());
        RecipeDTO recipeDTO = recipeOrder.getRecipe();
        Recipe recipeToCreate = new Recipe();
        List<Medicament> medicaments = medicamentRepository.findAllById(recipeOrder.getMedicamentIDs());

        recipeToCreate.setPatient(patient.get());
        recipeToCreate.setPatientName(recipeDTO.getPatientName());
        recipeToCreate.setE_card(recipeDTO.getE_card());
        recipeToCreate.setDateOfIssue(recipeDTO.getDateOfIssue());
        recipeToCreate.setPatientAddress(recipeDTO.getPatientAddress());
        recipeToCreate.setRecipeNumber(recipeDTO.getRecipeNumber());
        recipeToCreate.setMedicamentList(new ArrayList<>());

        patient.get().getRecipes().add(recipeToCreate);

        patientRepository.save(patient.get());
        recipeRepository.save(recipeToCreate);

        for (Medicament medicament : medicaments){
            medicament.getRecipe().add(recipeToCreate);
            recipeToCreate.getMedicamentList().add(medicament);
            medicamentRepository.save(medicament);
        }
        recipeRepository.save(recipeToCreate);

    }


public List<MedicamentByRecipeDTO> getAllMedicamentByRecipe(int recipeID){
        Optional<Recipe> recipe = recipeRepository.findById(recipeID);
        List<MedicamentByRecipeDTO> medicamentList = new ArrayList<>();

        recipe.get().getMedicamentList().forEach(
                medicament -> {
                    MedicamentByRecipeDTO tempObject = new MedicamentByRecipeDTO();
                    tempObject.setMedicamentId(medicament.getMedicamentId());
                    tempObject.setDose(medicament.getDose());
                    tempObject.setName(medicament.getName());
                    tempObject.setProducer(medicament.getProducer());
                    medicamentList.add(tempObject);
                }
        );
        return medicamentList;

}



}
