package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "recipeId")
@Entity
public class Recipe {




    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, unique = true)
    private int recipeId;

    @ManyToOne
    @JoinColumn(name = "patientId")
    private Patient patient;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "RecipeMedicament",
            joinColumns = @JoinColumn(name = "recipeId"),
            inverseJoinColumns = @JoinColumn(name = "medicamentId")
    )
    private List<Medicament> medicamentList;


    @Column(nullable = false)
    private String patientName;

    @Column(nullable = false)
    private int e_card;

    @Column(nullable = false)
    private String dateOfIssue;

    @Column(nullable = false)
    private String patientAddress;

    @Column(nullable = false)
    private int recipeNumber;

    public Recipe(String patientName, int e_card, String dateOfIssue, String patientAddress, int recipeNumber) {
        this.patientName = patientName;
        this.e_card = e_card;
        this.dateOfIssue = dateOfIssue;
        this.patientAddress = patientAddress;
        this.recipeNumber = recipeNumber;
    }
}
