package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.TimePlanResponseDTO;
import com.managementSystem.clinic.model.Employee;
import com.managementSystem.clinic.model.Timetable;
import com.managementSystem.clinic.repository.EmployeeRepository;
import com.managementSystem.clinic.repository.TimePlanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TimeTableService {




    private final EmployeeRepository employeeRepository;


    private final TimePlanRepository timePlanRepository;

    // Time plan Secretary
    public void addTimePlanToEmployee(int employeeID, Timetable timetable) {
        Employee employee =  employeeRepository.getReferenceById(employeeID);

        employee.getTimetableList().add(timetable);
        timetable.setEmployee(employee);

        timePlanRepository.save(timetable);
        employeeRepository.save(employee);
    }


    public ArrayList<TimePlanResponseDTO> getAllTimePlansOfEmployee(int employeeID) {
        Optional<Employee> employee =  employeeRepository.findById(employeeID);
        ArrayList<TimePlanResponseDTO> timePlanResponseDTOS = new ArrayList<>();

        employee.get().getTimetableList().forEach(timetable -> {
            TimePlanResponseDTO tempObject = new TimePlanResponseDTO();
            tempObject.setValuesFromSource(timetable);
            timePlanResponseDTOS.add(tempObject);
        });

        return timePlanResponseDTOS;

    }



    public void updateTimePlan(Timetable update) {
        Timetable timetableToEdit = timePlanRepository.getReferenceById(update.getTimeTableId());
        timetableToEdit.setValuesFormSource(update);
        timePlanRepository.save(timetableToEdit);
    }


    public void deleteTimePlan(int employeeID, int timeTableID) {
        timePlanRepository.deleteById(timeTableID);
        Employee employee =  employeeRepository.getReferenceById(employeeID);

        for (Timetable timePlan : employee.getTimetableList()) {
            if (timePlan.getTimeTableId() == timeTableID) {
                employee.getTimetableList().remove(timePlan);
            }
        }
    }

}
