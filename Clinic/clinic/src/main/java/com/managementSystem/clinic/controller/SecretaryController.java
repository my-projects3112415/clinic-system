package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.SecretaryResponseDTO;
import com.managementSystem.clinic.model.Assistant;
import com.managementSystem.clinic.model.Patient;
import com.managementSystem.clinic.model.Recipe;
import com.managementSystem.clinic.model.Secretary;
import com.managementSystem.clinic.repository.PatientRepository;
import com.managementSystem.clinic.service.AssistantService;
import com.managementSystem.clinic.service.PatientService;
import com.managementSystem.clinic.service.SecretaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/")
public class SecretaryController {

    private final SecretaryService secretaryService;


    // Secretary Requests
    @PostMapping("addSecretary")
    public ResponseEntity<String> addSecretary(@RequestBody Secretary secretary){
        try {
            secretaryService.addSecretary(secretary);
            return ResponseEntity.ok("Secretary has been added");
        } catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }
    }

    @PutMapping("L2/editSecretary/{id}")
    public ResponseEntity<String> editSecretary(@PathVariable int id, @RequestBody Secretary secretary){
        try {
            secretaryService.editSecretary(id, secretary);
            return ResponseEntity.ok("Secretary has been edited");
        } catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }

    }

    @GetMapping("L2/getSecretaryBy/{id}")
    public ResponseEntity<SecretaryResponseDTO> getSecretaryById(@PathVariable int id){
        try {
            return ResponseEntity.ok(secretaryService.getSecretaryById(id));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @DeleteMapping("deleteSecretaryBy/{id}")
    public ResponseEntity<String> deleteSecretaryById(@PathVariable int id){
        try {
            secretaryService.deleteSecretaryById(id);
            return ResponseEntity.ok("Secretary has been deleted");
        } catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }
    }


    @GetMapping("getAllSecretaries")
    public ResponseEntity<List<SecretaryResponseDTO>> getAllSecretaries(){
        try {
            return ResponseEntity.ok(secretaryService.getAllSecretaries());
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }







}
