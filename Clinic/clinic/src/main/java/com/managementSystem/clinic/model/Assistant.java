package com.managementSystem.clinic.model;



import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.managementSystem.clinic.Enums.FunctionalStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
//@JsonIdentityInfo Deal with all references in the background ( ManagedRef , Backref )
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "employeeID")
//@PrimaryKeyJoinColumn(name = "assistantId", referencedColumnName = "employeeID")
public class Assistant extends Employee{

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(updatable = false, unique = true)
//    private int assistantId;


//    //    @JsonManagedReference
//    @OneToMany(mappedBy = "assistant")
//    private List<TimetableAsi> timetableAsiList;




    public Assistant(String firstName, String lastname, int tel,
                     String address,FunctionalStatus functionalStatus, String username, String password) {
        super(firstName, lastname,tel,address, functionalStatus,username,password);
    }








//
//    @Column(nullable = false)
//    private String firstName;
//
//    @Column(nullable = false)
//    private String lastname;
//
//    @Column(nullable = false)
//    private int tel;
//
//    @Column(nullable = false)
//    private String address;
//
//    @Enumerated(EnumType.STRING)
//    @Column(nullable = false)
//    private FunctionalStatus functionalStatus;

//    public Assistant(String firstName, String lastname, int tel, String address,FunctionalStatus functionalStatus) {
//        this.firstName = firstName;
//        this.lastname = lastname;
//        this.tel = tel;
//        this.address = address;
//        this.functionalStatus = functionalStatus;
//    }

}
