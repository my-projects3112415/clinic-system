package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.model.Recipe;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RecipeRequestDTO {
    private int patientID;
    private RecipeDTO recipe;
    private List<Integer> medicamentIDs;
}
