package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<Invoice,Integer> {
}
