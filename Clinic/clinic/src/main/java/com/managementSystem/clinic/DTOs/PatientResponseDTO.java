package com.managementSystem.clinic.DTOs;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientResponseDTO {
    private int patientID;
    private String firstName;
    private String lastname;
    private int tel;
    private String address;
    private String birthday;
    private long e_card;

}
