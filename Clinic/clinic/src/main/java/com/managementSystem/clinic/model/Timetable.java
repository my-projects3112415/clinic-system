package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "timeTableId")

public class Timetable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, unique = true)
    private int timeTableId;


    @ManyToOne
    @JoinColumn(name = "employeeID")
    private Employee employee;


    public Timetable(int startDate, int endDate, float monStart, float monEnd, float tuesStart, float tuesEnd, float wedStart, float wedEnd, float thursStart, float thursEnd, float friStart, float friEnd, float satStart, float satEnd) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.monStart = monStart;
        this.monEnd = monEnd;
        this.tuesStart = tuesStart;
        this.tuesEnd = tuesEnd;
        this.wedStart = wedStart;
        this.wedEnd = wedEnd;
        this.thursStart = thursStart;
        this.thursEnd = thursEnd;
        this.friStart = friStart;
        this.friEnd = friEnd;
        this.satStart = satStart;
        this.satEnd = satEnd;
    }

    @Column(nullable = false)
    private int startDate;


    @Column(nullable = false)
    private int endDate;


    @Column
    private float monStart;

    @Column
    private float monEnd;


    @Column
    private float tuesStart;

    @Column
    private float tuesEnd;


    @Column
    private float wedStart;

    @Column
    private float wedEnd;


    @Column
    private float thursStart;

    @Column
    private float thursEnd;


    @Column
    private float friStart;

    @Column
    private float friEnd;


    @Column
    private float satStart;

    @Column
    private float satEnd;


    public void setValuesFormSource(Timetable source) {

        this.startDate = source.getStartDate();
        this.endDate = source.getEndDate();

        this.monStart = source.getMonStart();
        this.monEnd = source.getMonEnd();

        this.tuesStart = source.getTuesStart();
        this.tuesEnd = source.getTuesEnd();

        this.thursStart = source.getThursStart();
        this.thursEnd = source.getThursEnd();


        this.satStart = source.getSatStart();
        this.satEnd = source.getSatEnd();


        this.friStart = source.getFriStart();
        this.friEnd = source.getFriEnd();


        this.wedStart = source.getWedStart();
        this.wedEnd = source.getWedEnd();


    }


}
