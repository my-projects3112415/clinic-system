package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Secretary;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SecretaryRepository extends JpaRepository<Secretary, Integer> {


}
