package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.MedicamentDTO;
import com.managementSystem.clinic.model.Medicament;
import com.managementSystem.clinic.repository.MedicamentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MedicamentService {


    private final MedicamentRepository medicamentRepository;



    public List<MedicamentDTO> getAllMedicamments(){
        List<MedicamentDTO> medicamentDTOS = new ArrayList<>();
        medicamentRepository.findAll().forEach(
                medicament -> {
                    MedicamentDTO tempObject = new MedicamentDTO();
                    tempObject.setMedicamentId(medicament.getMedicamentId());
                    tempObject.setName(medicament.getName());
                    tempObject.setProducer(medicament.getProducer());
                    tempObject.setDose(medicament.getDose());
                    medicamentDTOS.add(tempObject);
                }
        );

        return medicamentDTOS;
    }




}
