package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.model.DocService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PatientInvoiceDTO {

    private int invoiceId;
    private List<DocServiceDTO> services;
    private String patientName;
    private String secretaryName;
    private String dataOfInvoice;
    private int invoiceNumber;
    private long e_Card;

}
