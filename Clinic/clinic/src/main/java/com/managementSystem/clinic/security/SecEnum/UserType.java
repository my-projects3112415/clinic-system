package com.managementSystem.clinic.security.SecEnum;

public enum UserType {

    ADMIN,
    SECRETARY,
    ASSISTANT
}
