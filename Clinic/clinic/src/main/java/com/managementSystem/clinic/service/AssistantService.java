package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.AssistantResponseDTO;
import com.managementSystem.clinic.model.Assistant;
import com.managementSystem.clinic.model.Employee;
import com.managementSystem.clinic.repository.AssistantRepository;
import com.managementSystem.clinic.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AssistantService {



    private final AssistantRepository assistantRepository;
    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder passwordEncoder;


    public void addAssistant(Assistant assistant) {
        assistantRepository.save(assistant);
    }


    public List<AssistantResponseDTO> getAllAssistants(){
        ArrayList<Assistant> assistants = new ArrayList<>(assistantRepository.findAll());
        ArrayList<AssistantResponseDTO> assistantResponseDTOS = new ArrayList<>();

        assistants.forEach(
                assistant -> {
                    AssistantResponseDTO tempObject = new AssistantResponseDTO();
                    tempObject.setEmployeeID(assistant.getEmployeeID());
                    tempObject.setFirstName(assistant.getFirstName());
                    tempObject.setLastname(assistant.getLastname());
                    tempObject.setAddress(assistant.getAddress());
                    tempObject.setTel(assistant.getTel());
                    tempObject.setTimetableList(assistant.getTimetableList());
                    tempObject.setFunctionalStatus(assistant.getFunctionalStatus());
                    tempObject.setPassword(assistant.getPassword());
                    tempObject.setUserName(assistant.getUserName());
                    assistantResponseDTOS.add(tempObject);
                }
        );

        return assistantResponseDTOS;
    }



    // NOT IN USE
    public Optional<Assistant> getAssistantById(int id) {
        return assistantRepository.findById(id);
    }

    public ResponseEntity<String> editAssistantById(int id, Assistant assistant) {


        if (myOwnAccount(id)){
            Optional<Assistant> assistantToEdit = assistantRepository.findById(id);
            assistantToEdit.get().setFirstName(assistant.getFirstName());
            assistantToEdit.get().setLastname(assistant.getLastname());
            assistantToEdit.get().setTel(assistant.getTel());
            assistantToEdit.get().setAddress(assistant.getAddress());
            assistantToEdit.get().setUserName(assistant.getUserName());

            if (isAdmin()){
                assistantToEdit.get().setFunctionalStatus(assistant.getFunctionalStatus());
            }



            if (!assistant.getPassword().isEmpty() || !assistant.getPassword().isBlank()){
                String hashedPassword = passwordEncoder.encode(assistant.getPassword());
                assistantToEdit.get().setPassword(hashedPassword);
            }

            assistantRepository.save(assistantToEdit.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }else  {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }


    private Boolean myOwnAccount(int id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String role = authentication.getAuthorities().toString();
        String username = authentication.getName();
        Optional<Employee> loggedInUser = employeeRepository.findUserByName(username);
        // roleToCheck most be in this form ( SCOPE_ADMIN )
        return ( loggedInUser.get().getEmployeeID() == id && Objects.equals(role, "[SCOPE_ASSISTANT]") || Objects.equals(role,"[SCOPE_ADMIN]") );
    }
   private Boolean isAdmin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String role = authentication.getAuthorities().toString();
        String username = authentication.getName();
        Optional<Employee> loggedInUser = employeeRepository.findUserByName(username);
        // roleToCheck most be in this form ( SCOPE_ADMIN )
        return ( Objects.equals(role,"[SCOPE_ADMIN]") );
    }

    public void deleteAssistantById(int id){
        assistantRepository.deleteById(id);
    }


}
