package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.DocService;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocServiceRepository extends JpaRepository<DocService,Integer> {

}
