package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.managementSystem.clinic.Enums.FunctionalStatus;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@Entity
//@JsonIdentityInfo Deal with all references in the background ( ManagedRef , Backref )
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "employeeID")
//@PrimaryKeyJoinColumn(name = "doctorId", referencedColumnName = "employeeID")
public class Doctor extends Employee{





    public Doctor( String firstName, String lastname, int tel, String address,
                     FunctionalStatus functionalStatus, String username, String password)  {
        super(firstName,lastname,tel,address,functionalStatus,username,password);
    }



}
