package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.model.Timetable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TimePlanResponseDTO {
    private int timeTableId;

    private float startDate;
    private float endDate;

    private float monStart;
    private float monEnd;

    private float tuesStart;
    private float tuesEnd;

    private float wedStart;
    private float wedEnd;

    private float thursStart;
    private float thursEnd;

    private float friStart;
    private float friEnd;

    private float satStart;
    private float satEnd;

    public void setValuesFromSource(Timetable source){
        this.timeTableId = source.getTimeTableId();

        this.startDate = source.getStartDate();
        this.endDate = source.getEndDate();

        this.monStart = source.getMonStart();
        this.monEnd = source.getMonEnd();

        this.thursStart = source.getThursStart();
        this.thursEnd = source.getThursEnd();

        this.wedStart = source.getWedStart();
        this.wedEnd = source.getWedEnd();

        this.friStart = source.getFriStart();
        this.friEnd = source.getFriEnd();

        this.satStart = source.getSatStart();
        this.satEnd = source.getSatEnd();

        this.tuesStart = source.getTuesStart();
        this.tuesEnd = source.getTuesEnd();

    }
}
