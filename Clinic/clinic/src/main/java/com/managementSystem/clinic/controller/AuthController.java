package com.managementSystem.clinic.controller;


import com.managementSystem.clinic.DTOs.LoginDataUpdateDTO;
import com.managementSystem.clinic.DTOs.RestPasswordDTO;
import com.managementSystem.clinic.model.Employee;
import com.managementSystem.clinic.repository.EmployeeRepository;
import com.managementSystem.clinic.security.SecDTO.LoginDTO;
import com.managementSystem.clinic.security.SecDTO.RegisterDTO;
import com.managementSystem.clinic.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@CrossOrigin
@RequiredArgsConstructor
public class AuthController {


    private final AuthService authService;
    private final EmployeeRepository employeeRepository;


    @PostMapping("api/Admin/Register")
    public ResponseEntity<String> registerDoctor(@RequestBody RegisterDTO registerDto) {
        try{
            return authService.createDoctor(registerDto);

        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("api/secretary/register")
    public ResponseEntity<String> registerSecretary(@RequestBody RegisterDTO registerDTO){
        try{
            return authService.createSecretary(registerDTO);
        }catch (Exception e ){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("api/L2/assistant/register")
    public ResponseEntity<String> registerAssistant(@RequestBody RegisterDTO registerDTO){
        try {
            return authService.createAssistant(registerDTO);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @PutMapping("auth/updateLoginData")
    public ResponseEntity<String> updateLoginData(@RequestBody LoginDataUpdateDTO loginDataUpdateDTO){
        try{
            return authService.updateLoginData(loginDataUpdateDTO);
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR );
        }
    }


    @PutMapping("public/resetPassword")
    public ResponseEntity<String> resetPassword(@RequestBody RestPasswordDTO restPasswordDTO){
        try {
            return authService.restPassword(restPasswordDTO);
        }catch (Exception e){
            return new  ResponseEntity<>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("auth/getAccount/{username}")
    public ResponseEntity<Optional<Employee>> getMyAccount(@PathVariable String username){
//        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        return ResponseEntity.ok(employeeRepository.findUserByName(username));
    }



    @PostMapping("public/login")
    public ResponseEntity<String> login(@RequestBody LoginDTO loginDTO) {
        try{

            return authService.login(loginDTO);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
