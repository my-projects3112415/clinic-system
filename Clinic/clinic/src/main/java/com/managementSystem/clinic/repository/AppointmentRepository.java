package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface AppointmentRepository extends JpaRepository<Appointment,Integer> {

    @Query(value = "SELECT * FROM APPOINTMENT where patientId = :patientId", nativeQuery = true)
    Set<Appointment> findAppointmentsByPatientID(@Param("patientId") int patientID);
}
