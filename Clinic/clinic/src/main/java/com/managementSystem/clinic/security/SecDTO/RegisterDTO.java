package com.managementSystem.clinic.security.SecDTO;

import com.managementSystem.clinic.model.Assistant;
import com.managementSystem.clinic.model.Doctor;
import com.managementSystem.clinic.model.Secretary;
import com.managementSystem.clinic.security.SecEnum.UserType;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@Data
public class RegisterDTO {
    private Secretary secretary;
    private Assistant assistant;
    private Doctor doctor;

}
