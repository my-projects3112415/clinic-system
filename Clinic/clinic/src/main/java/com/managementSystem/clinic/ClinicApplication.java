package com.managementSystem.clinic;

import com.managementSystem.clinic.Enums.FunctionalStatus;
import com.managementSystem.clinic.model.*;
import com.managementSystem.clinic.repository.*;
import com.managementSystem.clinic.security.SecEnum.UserType;
import com.managementSystem.clinic.security.SecRepository.RoleRepository;
import com.managementSystem.clinic.security.configuration.RSAKeyPair;
import com.managementSystem.clinic.security.usersModel.RoleEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.SecureRandom;
import java.util.Collections;


@EnableConfigurationProperties(RSAKeyPair.class)
@SpringBootApplication
public class ClinicApplication implements CommandLineRunner {

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    AppointmentRepository appointmentRepository;

    @Autowired
    AssistantRepository assistantRepository;

    @Autowired
    MedicamentRepository medicamentRepository;

    @Autowired
    SecretaryRepository secretaryRepository;


    @Autowired
    DocServiceRepository docServiceRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    DoctorRepository doctorRepository;


    public static String key = "ta+kh!LWL<:4Qfs/}Hz?$w7Mgk#Ew-*7t'fk]FU#$:0b2796p9c5L*~@Z9JEX";


    public static void main(String[] args) {
        SpringApplication.run(ClinicApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        String hashedPassword = passwordEncoder.encode("12345");

        Patient patient = new Patient("Max", "Goldenberg", 126757573, randomText(10), "30.01.1998", 1111113111L);
        Patient patient1 = new Patient(randomText(5), "Nalona", 126757573, randomText(8), "30.01.1998", 1711111111L);
        Patient patient4 = new Patient(randomText(5), "Markov", 126757573, randomText(7), "30.01.1998", 1111144111L);
        Patient patient2 = new Patient(randomText(5), "Santa", 126757573, randomText(10), "30.01.1998", 1111119911L);

        Assistant assistant = new Assistant(randomText(7), "Shara", 11111111, randomText(10), FunctionalStatus.EMPLOYED, "test1", hashedPassword);
        Assistant assistant1 = new Assistant(randomText(5), randomText(10), 11111, randomText(10), FunctionalStatus.EMPLOYED, "test2", hashedPassword);
        Assistant assistant2 = new Assistant(randomText(6), randomText(10), 11111, randomText(10), FunctionalStatus.QUIT, "test3", hashedPassword);


        Medicament medicament = new Medicament("Pro", 2000, "Pfizer");
        Medicament medicament1 = new Medicament("Blanum", 2000, "Pfizer");
        Medicament medicament2 = new Medicament("Mazakov", 2000, "Pfizer");


        Secretary secretary = new Secretary(randomText(6), randomText(5), 1111111111, randomText(10), FunctionalStatus.EMPLOYED, "test4", hashedPassword);
        Secretary secretary1 = new Secretary(randomText(3), randomText(6), 1111111111, randomText(10), FunctionalStatus.QUIT, "test5", hashedPassword);


        Doctor doctor = new Doctor("Ghaith", "Ghanoum", 1111111111, randomText(10), FunctionalStatus.EMPLOYED, "ghaith", hashedPassword);

        DocService docService = new DocService("Bis Morgen");
        DocService docService1 = new DocService("Bis Heute");
        DocService docService2 = new DocService("Bis Gestern");


//		Important
        RoleEntity adminRole = new RoleEntity(UserType.ADMIN);
        RoleEntity secretaryRole = new RoleEntity(UserType.SECRETARY);
        RoleEntity assistantRole = new RoleEntity(UserType.ASSISTANT);


        // Roles setup
        assistant.setRoleEntityList(Collections.singletonList(assistantRole));
        assistant1.setRoleEntityList(Collections.singletonList(assistantRole));
        assistant2.setRoleEntityList(Collections.singletonList(assistantRole));

        secretary.setRoleEntityList(Collections.singletonList(secretaryRole));
        secretary1.setRoleEntityList(Collections.singletonList(secretaryRole));

        doctor.setRoleEntityList(Collections.singletonList(adminRole));


        roleRepository.save(adminRole);
        roleRepository.save(secretaryRole);
        roleRepository.save(assistantRole);


        medicamentRepository.save(medicament);
        medicamentRepository.save(medicament1);
        medicamentRepository.save(medicament2);

        patientRepository.save(patient);
        patientRepository.save(patient1);
        patientRepository.save(patient2);
        patientRepository.save(patient4);


        doctorRepository.save(doctor);

        assistantRepository.save(assistant);
        assistantRepository.save(assistant1);
        assistantRepository.save(assistant2);

        secretaryRepository.save(secretary);
        secretaryRepository.save(secretary1);


        docServiceRepository.save(docService);
        docServiceRepository.save(docService1);
        docServiceRepository.save(docService2);

    }

    private String randomText(int length) {
        String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // Characters to choose from
        SecureRandom secureRandom = new SecureRandom();
        StringBuilder randomText = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            int randomIndex = secureRandom.nextInt(charset.length());
            char randomChar = charset.charAt(randomIndex);
            randomText.append(randomChar);
        }
        return  randomText.toString();
    }


}
