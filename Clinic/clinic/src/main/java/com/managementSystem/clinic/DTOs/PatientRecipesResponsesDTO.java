package com.managementSystem.clinic.DTOs;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class PatientRecipesResponsesDTO {
    private int recipeId;
    private List<MedicamentDTO> medicamentList;
    private String patientName;
    private String patientAddress;
    private String dateOfIssue;
    private int recipeNumber;
    private int e_card;
}
