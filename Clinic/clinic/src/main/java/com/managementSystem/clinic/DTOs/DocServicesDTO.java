package com.managementSystem.clinic.DTOs;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DocServicesDTO {
    private int docServiceId;
    private String type;
    private float price;

}
