package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Medicament;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicamentRepository extends JpaRepository<Medicament,Integer> {
}
