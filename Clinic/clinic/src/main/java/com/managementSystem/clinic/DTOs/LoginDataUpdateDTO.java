package com.managementSystem.clinic.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginDataUpdateDTO {
    private int accountID;
    private String newUsername;
    private String currentPassword;
    private String newPassword;
    private String confirmNewPassword;
}
