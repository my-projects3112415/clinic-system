package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "docServiceId")

@Entity
public class DocService {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, unique = true)
    private int docServiceId;


    @ManyToMany
    @JoinColumn(name = "invoiceId")
    private List<Invoice> invoices;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false)
    private float price;


    public DocService(String type) {
        this.type = type;
    }
}
