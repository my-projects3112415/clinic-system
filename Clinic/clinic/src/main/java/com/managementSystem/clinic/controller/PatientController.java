package com.managementSystem.clinic.controller;


import com.managementSystem.clinic.DTOs.PatientResponseSecDTO;
import com.managementSystem.clinic.DTOs.PatientResponseDTO;
import com.managementSystem.clinic.model.Patient;
import com.managementSystem.clinic.service.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/L2")
public class PatientController {

    private final PatientService patientService;





    // Patient Requests
    @PostMapping("addPatient")
    public ResponseEntity<String> addPatient(@RequestBody Patient patient){
        try {
            patientService.addPatient(patient);
            return ResponseEntity.ok( "Patient has been added!");

        }catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }
    }

    @GetMapping("getPatientBy/{id}")
    public ResponseEntity<Optional<Patient>> getPatientById(@PathVariable int id){
        try {
            return ResponseEntity.ok(patientService.getPatientById(id));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


    @PutMapping("editPatient/{id}")
    public ResponseEntity<String> editPatient(@PathVariable int id, @RequestBody Patient patient){
        try {
            patientService.editPatient(id, patient);
            return ResponseEntity.ok("Patient has been edited");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    // WE DON'T DELETE PATIENTS =>  ERROR IN DB
    @DeleteMapping("deletePatientBy/{id}")
    public ResponseEntity<String> deletePatient(@PathVariable int id){
        try {
            patientService.deletePatientById(id);
            return ResponseEntity.ok("Patient has been deleted");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

    }



    @GetMapping("getPatientByLastname/{lastname}")
    public ResponseEntity<List<PatientResponseSecDTO>> getPatientByLastname(@PathVariable String lastname){
        try {
            return ResponseEntity.ok(patientService.getPatientByLastname(lastname));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }




    @GetMapping("getAllPatient")
    public ResponseEntity<List<PatientResponseDTO>> getAllPatient(){
        try {
            return ResponseEntity.ok(patientService.getAllPatient());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }



    @GetMapping("getPatientByE_Card/{e_card}")
    public ResponseEntity<PatientResponseSecDTO> getPatientByE_Card(@PathVariable int e_card){
        try {
            return ResponseEntity.ok(patientService.getPatientByE_Card(e_card));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }














}
