package com.managementSystem.clinic.service;


import com.managementSystem.clinic.DTOs.SecretaryResponseDTO;
import com.managementSystem.clinic.model.Employee;
import com.managementSystem.clinic.model.Secretary;
import com.managementSystem.clinic.repository.EmployeeRepository;
import com.managementSystem.clinic.repository.SecretaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SecretaryService {



    private final SecretaryRepository secretaryRepository;
    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder passwordEncoder;



    // Secretary Requests
    public void addSecretary(Secretary secretary) {
         secretaryRepository.save(secretary);
    }

    public ResponseEntity<String> editSecretary(int id, Secretary secretary){

        if (selfOrSecretary(id)){
            Optional<Secretary> secretaryToEdit = secretaryRepository.findById(id);
            secretaryToEdit.get().setFirstName(secretary.getFirstName());
            secretaryToEdit.get().setLastname(secretary.getLastname());
            secretaryToEdit.get().setAddress(secretary.getAddress());
            secretaryToEdit.get().setTel(secretary.getTel());
            secretaryToEdit.get().setUserName(secretary.getUserName());

            if (isAdmin()){
                secretaryToEdit.get().setFunctionalStatus(secretary.getFunctionalStatus());
            }

            if (!secretary.getPassword().isEmpty() || !secretary.getPassword().isBlank()){
                String hashedPassword = passwordEncoder.encode(secretary.getPassword());
                secretaryToEdit.get().setPassword(hashedPassword);
            }

            secretaryRepository.save(secretaryToEdit.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    private Boolean selfOrSecretary(int id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String role = authentication.getAuthorities().toString();
        String username = authentication.getName();
        Optional<Employee> loggedInUser = employeeRepository.findUserByName(username);
        // roleToCheck most be in this form ( SCOPE_ADMIN )
        return ( loggedInUser.get().getEmployeeID() == id && Objects.equals(role, "[SCOPE_SECRETARY]") || Objects.equals(role, "[SCOPE_ADMIN]")  );
    }
    private Boolean isAdmin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String role = authentication.getAuthorities().toString();
        String username = authentication.getName();
        Optional<Employee> loggedInUser = employeeRepository.findUserByName(username);
        // roleToCheck most be in this form ( SCOPE_ADMIN )
        return  (Objects.equals(role, "[SCOPE_ADMIN]")  );
    }

    public List<SecretaryResponseDTO> getAllSecretaries(){
        List<Secretary> secretaries = secretaryRepository.findAll();
        List<SecretaryResponseDTO> responseDTOList = new ArrayList<>();

        secretaries.forEach( s -> {
            SecretaryResponseDTO tempoObject = new SecretaryResponseDTO();
            tempoObject.setSecretaryId(s.getEmployeeID());
            tempoObject.setFirstName(s.getFirstName());
            tempoObject.setLastname(s.getLastname());
            tempoObject.setAddress(s.getAddress());
            tempoObject.setTel(s.getTel());
            tempoObject.setTimetableList(s.getTimetableList());
            tempoObject.setFunctionalStatus(s.getFunctionalStatus());
            responseDTOList.add(tempoObject);
        });
        return responseDTOList;
    }


    // NOT IN USE
    public SecretaryResponseDTO getSecretaryById(int id) {
        SecretaryResponseDTO secretaryResponseDTO = new SecretaryResponseDTO();
        Optional<Secretary> secretary = secretaryRepository.findById(id);
        secretaryResponseDTO.setSecretaryId(secretary.get().getEmployeeID());
        secretaryResponseDTO.setFirstName(secretary.get().getFirstName());
        secretaryResponseDTO.setLastname(secretary.get().getLastname());
        secretaryResponseDTO.setAddress(secretary.get().getAddress());
        secretaryResponseDTO.setFunctionalStatus(secretary.get().getFunctionalStatus());
        secretaryResponseDTO.setTimetableList(secretary.get().getTimetableList());
        return secretaryResponseDTO;
    }


    public void deleteSecretaryById(int id) {
        secretaryRepository.deleteById(id);
    }



}
