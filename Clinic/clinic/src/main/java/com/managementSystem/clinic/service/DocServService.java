package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.DocServicesDTO;
import com.managementSystem.clinic.DTOs.UpdateServiceDTO;
import com.managementSystem.clinic.model.DocService;
import com.managementSystem.clinic.repository.DocServiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DocServService {



    private final DocServiceRepository docServiceRepository;

    public void addNewService(DocService docService){
        docServiceRepository.save(docService);
    }

    // NOT IN USE
    public void deleteService(int serviceID){
        docServiceRepository.deleteById(serviceID);
    }

    public void updateService(UpdateServiceDTO updateServiceDTO){
        DocService docServiceToEdit = docServiceRepository.getReferenceById(updateServiceDTO.getServiceID());
        docServiceToEdit.setType(updateServiceDTO.getType());
        docServiceToEdit.setPrice(updateServiceDTO.getPrice());
        docServiceRepository.save(docServiceToEdit);
    }


    public List<DocServicesDTO> getAllServices(){
        List<DocServicesDTO> docServicesDTOs = new ArrayList<>();
        docServiceRepository.findAll().forEach(
                docService -> {
                    DocServicesDTO tempObject = new DocServicesDTO();
                    tempObject.setDocServiceId(docService.getDocServiceId());
                    tempObject.setPrice(docService.getPrice());
                    tempObject.setType(docService.getType());
                    docServicesDTOs.add(tempObject);
                }
        );
        return docServicesDTOs ;
    }



}
