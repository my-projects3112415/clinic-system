package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.managementSystem.clinic.Enums.FunctionalStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "employeeID")
@Entity
//@PrimaryKeyJoinColumn(name = "secretaryId", referencedColumnName = "employeeID")
public class Secretary extends Employee{



//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(updatable = false, unique = true)
//    private int secretaryId;




    @OneToMany(mappedBy = "secretary")
    private List<Invoice> invoices;




        public Secretary(String firstName, String lastname, int tel, String address,
                         FunctionalStatus functionalStatus, String username, String password)  {
            super(firstName,lastname,tel,address,functionalStatus,username,password);
        }






//    @Column(nullable = false)
//    private String firstName;
//
//    @Column(nullable = false)
//    private String lastname;
//
//    @Column(nullable = false)
//    private int tel;
//
//    @Column(nullable = false)
//    private String address;
//
//    @Enumerated(EnumType.STRING)
//    @Column(nullable = false)
//    private FunctionalStatus functionalStatus;



//    // Profile Data
//    @Column(nullable = false, unique = true)
//    private String userName;
//
//    @Column(nullable = false)
//    private String password;
//
//
//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(
//            name = "UserRoles",
//            joinColumns = @JoinColumn(name = "userID"),
//            inverseJoinColumns = @JoinColumn(name = "roleID")
//    )
//    private List<RoleEntity> roleEntityList;

//    public Secretary(String firstName, String lastname, int tel, String address, FunctionalStatus functionalStatus) {
//        this.firstName = firstName;
//        this.lastname = lastname;
//        this.tel = tel;
//        this.address = address;
//        this.functionalStatus = functionalStatus;
//    }
}
