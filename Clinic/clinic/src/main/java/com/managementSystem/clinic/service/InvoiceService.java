package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.DocServiceDTO;
import com.managementSystem.clinic.DTOs.InvoiceRequestDTO;
import com.managementSystem.clinic.DTOs.PatientInvoiceDTO;
import com.managementSystem.clinic.model.DocService;
import com.managementSystem.clinic.model.Invoice;
import com.managementSystem.clinic.model.Patient;
import com.managementSystem.clinic.model.Secretary;
import com.managementSystem.clinic.repository.DocServiceRepository;
import com.managementSystem.clinic.repository.InvoiceRepository;
import com.managementSystem.clinic.repository.PatientRepository;
import com.managementSystem.clinic.repository.SecretaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service


@RequiredArgsConstructor
public class InvoiceService {


    private final PatientRepository patientRepository;


    private final SecretaryRepository secretaryRepository;


    private final DocServiceRepository docServiceRepository;


    private final InvoiceRepository invoiceRepository;


    public void makeInvoiceWithServices(InvoiceRequestDTO invoiceRequestDTO) {
        Patient patient = patientRepository.getReferenceById(invoiceRequestDTO.getPatientID());
        Secretary secretary = secretaryRepository.getReferenceById(invoiceRequestDTO.getSecretaryID());
        Invoice invoice = invoiceRequestDTO.getInvoice();
        List<DocService> docServiceList = docServiceRepository.findAllById(invoiceRequestDTO.getDocServiceList());

        invoice.setSecretary(secretary);
        invoice.setPatient(patient);
        invoice.setPatientName(patient.getFirstName() + patient.getLastname());
        invoice.setSecretaryName(secretary.getFirstName() + secretary.getLastname());
        invoice.setE_Card(patient.getE_card());

        for (DocService docService : docServiceList) {
            invoice.getDocServices().add(docService);
        }
        invoiceRepository.save(invoice);

        for (DocService docService : docServiceList) {
            docService.getInvoices().add(invoice);
            docServiceRepository.save(docService);
        }

        patient.getInvoices().add(invoice);


        patientRepository.save(patient);
        secretaryRepository.save(secretary);


    }


    public List<PatientInvoiceDTO> getAllInvoicesByID(int patientId) {
        //Patient patient = patientRepository.getReferenceById(patientId);

        Optional<Patient> patient = patientRepository.findById(patientId);

        List<PatientInvoiceDTO> invoiceList = new ArrayList<>();

        for (Invoice invoice : patient.get().getInvoices()) {

            // Create new empty Invoice
            PatientInvoiceDTO invoiceDTO = new PatientInvoiceDTO();
            List<DocServiceDTO> listOfDocServices = new ArrayList<>();


            // Fill up the empty Invoice with data
            invoiceDTO.setInvoiceId(invoice.getInvoiceId());

            // Change all DocService into DTO Services
            invoice.getDocServices().forEach(docService -> {

                DocServiceDTO serviceDTO = new DocServiceDTO();

                serviceDTO.setPrice(docService.getPrice());
                serviceDTO.setType(docService.getType());

                listOfDocServices.add(serviceDTO);

            });

            // Set the new Custom DocServices DTO into our Custom list
            invoiceDTO.setServices(listOfDocServices);

            invoiceDTO.setPatientName(invoice.getPatientName());
            invoiceDTO.setSecretaryName(invoice.getSecretaryName());
            invoiceDTO.setDataOfInvoice(invoice.getDataOfInvoice());
            invoiceDTO.setInvoiceNumber(invoice.getInvoiceNumber());
            invoiceDTO.setE_Card(invoice.getE_Card());

            // Add the new Invoice DTO to invoiceList
            invoiceList.add(invoiceDTO);

        }

        return invoiceList;

    }


}
