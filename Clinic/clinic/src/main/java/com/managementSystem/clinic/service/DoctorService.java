package com.managementSystem.clinic.service;

import com.managementSystem.clinic.DTOs.DoctorResponseDTO;
import com.managementSystem.clinic.model.Doctor;
import com.managementSystem.clinic.model.Employee;
import com.managementSystem.clinic.repository.DoctorRepository;
import com.managementSystem.clinic.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DoctorService {

    private final DoctorRepository doctorRepository;
    private final EmployeeRepository employeeRepository;
    private final PasswordEncoder passwordEncoder;


    public void addDoctor(Doctor doctor){
        doctorRepository.save(doctor);
    }

    public ResponseEntity<String> editDoctor(int id,Doctor doctor){
        // Check the Identity of Doctor. Only Logged Doctor can change his Info
        if (selfOrSuperAdmin(id)){
            Doctor doctorToEdit = doctorRepository.getReferenceById(id);
            doctorToEdit.setFirstName(doctor.getFirstName());
            doctorToEdit.setLastname(doctor.getLastname());
            doctorToEdit.setAddress(doctor.getAddress());
            doctorToEdit.setTel(doctor.getTel());
            doctorToEdit.setUserName(doctor.getUserName());

            // Main Doctor can not Quit
            if (id != 1){
                    doctorToEdit.setFunctionalStatus(doctor.getFunctionalStatus());
            }

            // Change Password only if there is new password entered
            if (!doctor.getPassword().isEmpty() || !doctor.getPassword().isBlank()){
                String hashedPassword = passwordEncoder.encode(doctor.getPassword());
                doctorToEdit.setPassword(hashedPassword);
            }

            doctorRepository.save(doctorToEdit);
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    public List<DoctorResponseDTO> getAllDoctors(){
        List<DoctorResponseDTO> doctorResponseDTOS = new ArrayList<>();
        doctorRepository.findAll().forEach(doctor -> {
            DoctorResponseDTO tempObject = new DoctorResponseDTO();
            tempObject.setEmployeeID(doctor.getEmployeeID());
            tempObject.setFirstName(doctor.getFirstName());
            tempObject.setLastname(doctor.getLastname());
            tempObject.setUserName(doctor.getUserName());
            tempObject.setAddress(doctor.getAddress());
            tempObject.setTel(doctor.getTel());
            tempObject.setFunctionalStatus(doctor.getFunctionalStatus());
            tempObject.setTimetableList(doctor.getTimetableList());
            doctorResponseDTOS.add(tempObject);
        });
        return doctorResponseDTOS;
    }


    private Boolean selfOrSuperAdmin(int id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Optional<Employee> loggedInUser = employeeRepository.findUserByName(username);
        // roleToCheck most be in this form ( SCOPE_ADMIN )
        return ( loggedInUser.get().getEmployeeID() == id || loggedInUser.get().getEmployeeID() == 1 );
    }


}
