package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "invoiceId")

@Entity
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, unique = true)
    private int invoiceId;


    @ManyToOne
    @JoinColumn(name = "patientId")
    private Patient patient;


    @ManyToMany
    @JoinTable(
            name = "InvoicePaper",
            joinColumns = @JoinColumn(name = "invoiceId"),
            inverseJoinColumns = @JoinColumn(name = "docServiceId")
    )
    private List<DocService> docServices;


    @ManyToOne
    @JoinColumn(name = "secretaryId")
    private Secretary secretary;


    @Column(nullable = false)
    private String patientName;

    @Column(nullable = false)
    private String secretaryName;

    @Column(nullable = false)
    private String dataOfInvoice;

    @Column(nullable = false)
    private int invoiceNumber;

    @Column(nullable = false)
    private long E_Card;

    public Invoice(String patientName, String secretaryName, String dataOfInvoice, int invoiceNumber) {
        this.patientName = patientName;
        this.secretaryName = secretaryName;
        this.dataOfInvoice = dataOfInvoice;
        this.invoiceNumber = invoiceNumber;
    }
}
