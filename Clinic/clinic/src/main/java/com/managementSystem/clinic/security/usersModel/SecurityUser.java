package com.managementSystem.clinic.security.usersModel;

import com.managementSystem.clinic.Enums.FunctionalStatus;
import com.managementSystem.clinic.model.Employee;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;


public class SecurityUser implements UserDetails {

    private final Employee user;

    public SecurityUser(Employee user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return user.getRoleEntityList()
                .stream()
                .map(r -> new SimpleGrantedAuthority(r.getType().name()))
                .toList();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return (user.getFunctionalStatus().name().equals(FunctionalStatus.EMPLOYED.name()));
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
