package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.MedicamentDTO;
import com.managementSystem.clinic.model.Medicament;
import com.managementSystem.clinic.service.MedicamentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/L2")
public class MedicamentController {

//    @Autowired
    private final MedicamentService medicamentService;

    @GetMapping("getAllMedicamments")
    public ResponseEntity<List<MedicamentDTO>> getAllMedicamments() {
        try {
            List<MedicamentDTO> medicaments = medicamentService.getAllMedicamments();
            return ResponseEntity.ok(medicaments);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

}
