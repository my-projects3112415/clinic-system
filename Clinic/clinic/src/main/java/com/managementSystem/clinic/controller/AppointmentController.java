package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.model.Appointment;
import com.managementSystem.clinic.service.AppointmentService;
import com.managementSystem.clinic.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("api/L2/")
public class AppointmentController {


    private final AppointmentService appointmentService;

    private final PatientService patientService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService, PatientService patientService) {
        this.appointmentService = appointmentService;
        this.patientService = patientService;
    }


    @PutMapping("addAppointmentToPatient/{id}")
    public ResponseEntity<String> addAppointmentToPatient(@PathVariable int id, @RequestBody Appointment appointment) {
        try {
            appointmentService.addAppointmentToPatient(id, appointment);
            return ResponseEntity.ok("Appointment has been added");
        } catch (Exception error) {
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }

    }

    @GetMapping("getAllAppointmentsByPatient/{id}")
    public ResponseEntity<Set<Appointment>> getAllAppointmentsByPatientID(@PathVariable int id) {
        try {
            return ResponseEntity.ok(appointmentService.getAllAppointmentsByPatientID(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PutMapping("deleteAppointmentFromPatient/{patientID}/{appointmentID}")
    public ResponseEntity<String> deleteAppointmentFromPatient(@PathVariable int patientID, @PathVariable int appointmentID) {
        try {
            appointmentService.deleteAppointmentFromPatient(patientID, appointmentID);
            return ResponseEntity.ok("Appointment has been deleted");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    @PutMapping("editAppointment")
    public ResponseEntity<String> editAppointment(@RequestBody Appointment appointment) {
        try {
            appointmentService.editAppointment(appointment);
            return ResponseEntity.ok("Appointment has been edited");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    // TODO Get Appointment be Date


}
