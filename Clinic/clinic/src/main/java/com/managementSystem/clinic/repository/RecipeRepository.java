package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeRepository extends JpaRepository<Recipe,Integer> {
}
