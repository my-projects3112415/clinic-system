package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.TimePlanResponseDTO;
import com.managementSystem.clinic.model.Timetable;
import com.managementSystem.clinic.service.TimeTableService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Set;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/")
public class TimeTableController {


    private final TimeTableService timeTableService;


    // Add Time Plan To Secretary
    @PutMapping("addTimePlanToEmployee/{employeeID}")
    public ResponseEntity<String> addTimePlanToEmployee(@PathVariable int employeeID, @RequestBody Timetable timetable) {
        try {
            timeTableService.addTimePlanToEmployee(employeeID, timetable);
            return ResponseEntity.ok("Time plan has been added");
        } catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }

    }

    @GetMapping("L2/getAllTimePlansOfEmployee/{id}")
    public ResponseEntity<ArrayList<TimePlanResponseDTO>> getAllPlansOfEmployeeByID(@PathVariable int id) {
        try {
            return ResponseEntity.ok(timeTableService.getAllTimePlansOfEmployee(id));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }

    @PutMapping("updateTimePlanEmployee")
    public ResponseEntity<String> updateTimePlan(@RequestBody Timetable timetable) {
        try {
            timeTableService.updateTimePlan(timetable);
            return ResponseEntity.ok("Time Plan has been updated");
        } catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }
    }

    @PutMapping("deleteTimePlanOfEmployee/{employeeID}/{timeTableID}")
    public ResponseEntity<String> deleteTimePlan(@PathVariable int employeeID, @PathVariable int timeTableID) {
        try {
            timeTableService.deleteTimePlan(employeeID, timeTableID);
            return ResponseEntity.ok("Time Plan has been deleted");
        } catch (Exception error){
            error.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error.getMessage());
        }

    }


}
