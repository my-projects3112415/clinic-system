package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.model.Patient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class RecipeDTO {
    private String patientName;
    private int e_card;
    private String dateOfIssue;
    private String patientAddress;
    private int recipeNumber;
    private List<MedicamentDTO> medicamentList;
}
