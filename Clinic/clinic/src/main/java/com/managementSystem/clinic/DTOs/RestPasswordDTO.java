package com.managementSystem.clinic.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RestPasswordDTO {
    private int accountID;
    private String key;
    private String newPassword;
    private String newPasswordConfirm;
}
