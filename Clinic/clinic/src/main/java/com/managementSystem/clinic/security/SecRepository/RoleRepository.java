package com.managementSystem.clinic.security.SecRepository;

import com.managementSystem.clinic.security.usersModel.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<RoleEntity,Integer> {



    @Query(value = "SELECT * FROM USER_ENTITY WHERE TYPE = :type",nativeQuery = true)
    Optional<RoleEntity> findByName(@Param("type") String type);
}
