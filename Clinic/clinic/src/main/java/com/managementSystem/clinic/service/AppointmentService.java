package com.managementSystem.clinic.service;

import com.managementSystem.clinic.model.Appointment;
import com.managementSystem.clinic.model.Patient;
import com.managementSystem.clinic.repository.AppointmentRepository;
import com.managementSystem.clinic.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AppointmentService {

    private final PatientRepository patientRepository;

    private final AppointmentRepository appointmentRepository;


    // Patient Requests
    public void addAppointmentToPatient(int id, Appointment appointment) {
        Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Patient not found"));

        appointment.setPatient(patient);
        patient.getAppointments().add(appointment);

        appointmentRepository.save(appointment);
        patientRepository.save(patient);

    }

    public Set<Appointment> getAllAppointmentsByPatientID(int id) {
        Set<Appointment> allPatientAppointments = new HashSet<>();
        Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Patient not found"));

        patient.getAppointments().forEach(allPatientAppointments::add);
        return allPatientAppointments;
    }


    public void deleteAppointmentFromPatient(int patientID, int appointmentID) {
        Patient patient = patientRepository.getReferenceById(patientID);
        appointmentRepository.deleteById(appointmentID);

        for (Appointment appointment : patient.getAppointments()) {
            if (appointment.getAppointmentId() == appointmentID) {
                patient.getAppointments().remove(appointment);
            }
        }
        patientRepository.save(patient);
    }


    //Not in Use
    public void editAppointment(Appointment update) {
        Appointment reference = appointmentRepository.getReferenceById(update.getAppointmentId());
        reference.setFromAppointment(update);
        appointmentRepository.save(reference);
    }


}
