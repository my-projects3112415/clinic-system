package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.Enums.FunctionalStatus;
import com.managementSystem.clinic.model.Timetable;
import com.managementSystem.clinic.security.usersModel.RoleEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Setter
@Getter
public class AssistantResponseDTO {
    private int employeeID;
    private String firstName;
    private String lastname;
    private long tel;
    private String address;
    private FunctionalStatus functionalStatus;
    private List<Timetable> timetableList;
    private String userName;
    private String password;
}
