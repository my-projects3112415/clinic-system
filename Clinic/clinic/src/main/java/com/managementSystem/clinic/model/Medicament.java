package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "medicamentId")

public class Medicament {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false, unique = true)
    private int medicamentId;

    @ManyToMany(mappedBy = "medicamentList",fetch = FetchType.EAGER)
    private List<Recipe> recipe;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false,length = 4)
    private int dose;

    @Column(nullable = false)
    private String producer;

    public Medicament(String name, int dose, String producer) {
        this.name = name;
        this.dose = dose;
        this.producer = producer;
    }
}
