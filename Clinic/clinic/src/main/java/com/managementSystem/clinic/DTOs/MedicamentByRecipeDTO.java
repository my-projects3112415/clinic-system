package com.managementSystem.clinic.DTOs;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MedicamentByRecipeDTO {
    private int medicamentId;
    private String name;
    private int dose;
    private String producer;

}
