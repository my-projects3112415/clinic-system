package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.Enums.FunctionalStatus;
import com.managementSystem.clinic.model.Timetable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DoctorResponseDTO {
    private int employeeID;
    private String firstName;
    private String lastname;
    private long tel;
    private String address;
    private FunctionalStatus functionalStatus;
    private List<Timetable> timetableList;
    private String userName;
}
