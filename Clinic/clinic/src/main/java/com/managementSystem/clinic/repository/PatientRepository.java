package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient,Integer> {


    @Query(value = "SELECT * FROM PATIENT where  LASTNAME = :name",nativeQuery = true)
    public List<Patient> getPatientByLastname(@Param("name") String name);



    @Query(value = "SELECT * FROM PATIENT Where E_CARD  = :e_card",nativeQuery = true )
    public Patient getPatientByE_Card(@Param("e_card") int e_card);




}
