package com.managementSystem.clinic.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PatientResponseSecDTO {

    private int patientId;
    private String firstName;
    private String lastname;
    private int tel;
    private String address;
    private String birthday;
    private Long e_card;

}
