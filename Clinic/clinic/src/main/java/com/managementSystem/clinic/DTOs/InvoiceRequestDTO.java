package com.managementSystem.clinic.DTOs;

import com.managementSystem.clinic.model.DocService;
import com.managementSystem.clinic.model.Invoice;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceRequestDTO {
    private int patientID;
    private int secretaryID;
    private Invoice invoice;
    private List<Integer> docServiceList;

}
