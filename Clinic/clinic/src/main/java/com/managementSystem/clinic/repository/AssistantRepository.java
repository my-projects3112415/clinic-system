package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Assistant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssistantRepository extends JpaRepository<Assistant,Integer> {
}
