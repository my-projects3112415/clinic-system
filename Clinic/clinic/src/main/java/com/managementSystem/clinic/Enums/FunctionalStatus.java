package com.managementSystem.clinic.Enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum FunctionalStatus {
    EMPLOYED,
    QUIT
}
