package com.managementSystem.clinic.security.configuration;


import com.managementSystem.clinic.security.SecService.JpaUserDetailsService;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.ImmutableSecret;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;

import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


import javax.crypto.spec.SecretKeySpec;

import java.util.Arrays;
import java.util.List;

import static org.springframework.security.config.Customizer.withDefaults;



// Important Notations to enable the Config
@Configuration
@CrossOrigin
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final RSAKeyPair rsaKeys;

    private final JpaUserDetailsService myUserDetailsService;


    // TO TEST USERS SECURITY IN CASH MEMORY
//    @Bean
//    public UserDetailsService userDetailsService() throws Exception {
//        // ensure the passwords are encoded properly
//        User.UserBuilder users = User.withDefaultPasswordEncoder();
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(users.username("assistant").password("password").roles("ASSISTANT").build());
//        manager.createUser(users.username("admin").password("password").roles("ADMIN").build());
//        return manager;
//    }


//    @Bean
//    public SecurityFilterChain allowEveryThing(HttpSecurity http) throws Exception {
//        return http
//                .csrf(AbstractHttpConfigurer::disable)
//                .headers(h -> h.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable))
//                .authorizeHttpRequests(auth -> auth.anyRequest().permitAll())
//                .build();
//    }



    /** This the Security Filter, which has Three Security Level up to the Role, Cors Config to access the App form the outside
     * We have enabled H2 Console only for test, we are using our own jwt Authentication, creating an unlimited Session Disabled the default Spring Forms  */
    @Bean
    public SecurityFilterChain mainApiFilterChain(HttpSecurity http) throws Exception {
        return http

                // Enable the Cros Config within the Filter
                .cors(Customizer.withDefaults())

                // Disable Csrf
                .csrf(AbstractHttpConfigurer::disable)

                // Filter Chain for all Requests inside the App
                .authorizeHttpRequests(auth ->
                {
                    auth.requestMatchers(new AntPathRequestMatcher("/h2/**")).permitAll();
                    auth.requestMatchers("/").permitAll();
                    auth.requestMatchers("/error").permitAll();
                    auth.requestMatchers(new AntPathRequestMatcher("/public/**")).permitAll();
                    auth.requestMatchers(new AntPathRequestMatcher("/auth/**")).hasAnyAuthority("SCOPE_ASSISTANT","SCOPE_SECRETARY","SCOPE_ADMIN");
                    auth.requestMatchers("/api/L2/L3/**").hasAnyAuthority("SCOPE_ASSISTANT","SCOPE_SECRETARY","SCOPE_ADMIN");
                    auth.requestMatchers("/api/L3/**").hasAnyAuthority("SCOPE_ASSISTANT","SCOPE_ADMIN");
                    auth.requestMatchers("/api/L2/**").hasAnyAuthority("SCOPE_SECRETARY","SCOPE_ADMIN");
                    auth.requestMatchers("/api/**").hasAuthority("SCOPE_ADMIN");

                })
                // Check the Auth using our Custom userDetailsService
                .userDetailsService(myUserDetailsService)

                // Disable by default spring login forms, We don't need then
                .headers(h -> h.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable))

                // Create Session with unlimited Time
                .sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS))

                // Check the jwt
                .oauth2ResourceServer(jwtCustomizer -> jwtCustomizer.jwt(withDefaults()))

                // Build everything
                .build();
    }


    /** This is the Cros Config to allow access from other ports ( outside ) inside it we enabled every kind of ( Headers, Methods and Ports )*/
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("*"));
        configuration.setAllowedMethods(List.of("*"));
        configuration.setAllowedHeaders(List.of("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


    @Bean
    JwtEncoder jwtEncoder() {
        return new NimbusJwtEncoder(
                new ImmutableJWKSet<>(
                        new JWKSet(
                                new RSAKey
                                        .Builder(rsaKeys.publicKey())
                                        .privateKey(rsaKeys.privateKey())
                                        .build()
                        )
                )
        );
    }

    @Bean
    public JwtDecoder jwtDecoder() {
        return NimbusJwtDecoder.withPublicKey(rsaKeys.publicKey()).build();
    }

//    @Bean
//    public JwtAuthenticationConverter jwtAuthenticationConverter() {
//        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
//        // removes SCOPE_ prefix from authorities
//        grantedAuthoritiesConverter.setAuthorityPrefix("");
//        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
//        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
//        return jwtAuthenticationConverter;
//    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}

