package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.managementSystem.clinic.Enums.FunctionalStatus;
import com.managementSystem.clinic.security.usersModel.RoleEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "employeeID")
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "employee_type")
public abstract class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, unique = true)
    private int employeeID;


    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastname;

    @Column(nullable = false)
    private long tel;

    @Column(nullable = false)
    private String address;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private FunctionalStatus functionalStatus;




    // Time Plan for each Employee
    @OneToMany(mappedBy = "employee")
    private List<Timetable> timetableList;


    // User Profile Data
    @Column(nullable = false, unique = true)
    private String userName;

    @Column(nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "UserRoles",
            joinColumns = @JoinColumn(name = "employeeID"),
            inverseJoinColumns = @JoinColumn(name = "roleID")
    )
    private List<RoleEntity> roleEntityList;


    public Employee(String firstName, String lastname, int tel, String address, FunctionalStatus functionalStatus, String userName, String password) {
        this.firstName = firstName;
        this.lastname = lastname;
        this.tel = tel;
        this.address = address;
        this.functionalStatus = functionalStatus;
        this.userName = userName;
        this.password = password;
    }
}
