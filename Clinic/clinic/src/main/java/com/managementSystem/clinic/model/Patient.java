package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "patientId")

@Entity
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, unique = true)
    private int patientId;


    @OneToMany(mappedBy = "patient")
    private List<Appointment> appointments;


    @OneToMany(mappedBy = "patient")
    private List<Invoice> invoices;


    @OneToMany(mappedBy = "patient")
    private List<Recipe> recipes;



    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastname;

    @Column(nullable = false)
    private int tel;

    @Column
    private String address;

    @Column(length = 10)
    private String birthday;

    @Column(length = 10, unique = true)
    private long e_card;

    public Patient(String firstName, String lastname, int tel, String address, String birthday, long e_card) {
        this.firstName = firstName;
        this.lastname = lastname;
        this.tel = tel;
        this.address = address;
        this.birthday = birthday;
        this.e_card = e_card;
    }
}
