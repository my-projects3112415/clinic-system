package com.managementSystem.clinic.service;


import com.managementSystem.clinic.ClinicApplication;
import com.managementSystem.clinic.DTOs.LoginDataUpdateDTO;
import com.managementSystem.clinic.DTOs.RestPasswordDTO;
import com.managementSystem.clinic.model.Assistant;
import com.managementSystem.clinic.model.Doctor;
import com.managementSystem.clinic.model.Employee;
import com.managementSystem.clinic.model.Secretary;
import com.managementSystem.clinic.repository.DoctorRepository;
import com.managementSystem.clinic.repository.EmployeeRepository;
import com.managementSystem.clinic.security.SecDTO.LoginDTO;
import com.managementSystem.clinic.security.SecDTO.RegisterDTO;
import com.managementSystem.clinic.security.SecRepository.RoleRepository;
import com.managementSystem.clinic.security.SecService.TokenService;
import com.managementSystem.clinic.security.usersModel.RoleEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {


    private final AuthenticationManager authenticationManager;

    private final PasswordEncoder passwordEncoder;

    private final EmployeeRepository employeeRepository;

    private final RoleRepository roleRepository;

    private final TokenService tokenService;

    private final SecretaryService secretaryService;

    private final AssistantService assistantService;

    private final DoctorRepository doctorRepository;






    public ResponseEntity<String> login(LoginDTO loginData) {

        // THis object testing if the given Information are Valid. We're passing the Username and Password using DTO
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginData.getUsername(),
                        loginData.getPassword()));


        // This Object holds the status, so that the User doesn't need to log in every request
        // SecurityContextHolder.getContext().setAuthentication(authentication);

        //Generate Token
        String token = tokenService.generateToken(authentication);


        //  String token = jwtGenerator.generateToken(authentication);
        //return new ResponseEntity<>(new AuthResponseDTO(token), HttpStatus.OK);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }


    public ResponseEntity<String> createDoctor(RegisterDTO registerData) {

        // Create Login Account
        if (!checkIfUsernameIsTaken(registerData.getDoctor().getUserName())) {
            Doctor doctor = new Doctor();

            doctor.setUserName(registerData.getDoctor().getUserName());
            doctor.setPassword(passwordEncoder.encode(registerData.getDoctor().getPassword()));

            RoleEntity roles = roleRepository.getReferenceById(1);

            doctor.setRoleEntityList(Collections.singletonList(roles));
            roleRepository.save(roles);

            roles.getUserEntities().add(doctor);

            doctor.setFirstName(registerData.getDoctor().getFirstName());
            doctor.setLastname(registerData.getDoctor().getLastname());
            doctor.setTel(registerData.getDoctor().getTel());
            doctor.setAddress(registerData.getDoctor().getAddress());
            doctor.setFunctionalStatus(registerData.getDoctor().getFunctionalStatus());

            doctorRepository.save(doctor);


            return new ResponseEntity<>("User registered success!", HttpStatus.OK);

        } else {

            return new ResponseEntity<>("Username is taken!", HttpStatus.BAD_REQUEST);
        }

    }

    public ResponseEntity<String> createSecretary(RegisterDTO registerData) {


        //Check Username
        if (!checkIfUsernameIsTaken(registerData.getSecretary().getUserName())) {
            //UserEntity secretary = new UserEntity();
            Secretary secretary = new Secretary();

            // Create Login Account
            secretary.setUserName(registerData.getSecretary().getUserName());
            secretary.setPassword(passwordEncoder.encode(registerData.getSecretary().getPassword()));

            RoleEntity roles = roleRepository.getReferenceById(2);

            secretary.setRoleEntityList(Collections.singletonList(roles));
            roleRepository.save(roles);

            roles.getUserEntities().add(secretary);



            //Create Information
            secretary.setFirstName(registerData.getSecretary().getFirstName());
            secretary.setLastname(registerData.getSecretary().getLastname());
            secretary.setTel(registerData.getSecretary().getTel());
            secretary.setAddress(registerData.getSecretary().getAddress());
            secretary.setFunctionalStatus(registerData.getSecretary().getFunctionalStatus());
            secretaryService.addSecretary(secretary);


            return new ResponseEntity<>("User registered success!", HttpStatus.OK);

        } else {

            return new ResponseEntity<>("Username is taken!", HttpStatus.BAD_REQUEST);
        }


    }

    public ResponseEntity<String> createAssistant(RegisterDTO registerData) {

        // Create Login Account
        if (!checkIfUsernameIsTaken(registerData.getAssistant().getUserName())) {
            Assistant assistant = new Assistant();

            assistant.setUserName(registerData.getAssistant().getUserName());
            assistant.setPassword(passwordEncoder.encode(registerData.getAssistant().getPassword()));

            RoleEntity roles = roleRepository.getReferenceById(3);

            assistant.setRoleEntityList(Collections.singletonList(roles));

            roleRepository.save(roles);

            roles.getUserEntities().add(assistant);
            assistant.setFirstName(registerData.getAssistant().getFirstName());
            assistant.setLastname(registerData.getAssistant().getLastname());
            assistant.setTel(registerData.getAssistant().getTel());
            assistant.setAddress(registerData.getAssistant().getAddress());
            assistant.setFunctionalStatus(registerData.getAssistant().getFunctionalStatus());

            assistantService.addAssistant(assistant);

            return new ResponseEntity<>("User registered success!", HttpStatus.OK);

        } else {

            return new ResponseEntity<>("Username is taken!", HttpStatus.BAD_REQUEST);
        }

    }

    public ResponseEntity<String> updateLoginData(LoginDataUpdateDTO loginDataUpdateDTO) {

        Optional<Employee> account = employeeRepository.findById(loginDataUpdateDTO.getAccountID());
       // UserEntity account = userRepository.getReferenceById(loginDataUpdateDTO.getAccountID());

        // Username Test
        if (!loginDataUpdateDTO.getNewUsername().isEmpty() && !loginDataUpdateDTO.getNewUsername().isBlank()){
            if (!checkIfUsernameIsTaken(loginDataUpdateDTO.getNewUsername())){
                account.get().setUserName(loginDataUpdateDTO.getNewUsername());
            }else {
                return new ResponseEntity<>("Username is taken", HttpStatus.BAD_REQUEST);
            }
        }

        // Password Data
        String enteredPassword = loginDataUpdateDTO.getCurrentPassword();
        String newPassword = loginDataUpdateDTO.getNewPassword();
        String newPasswordHashed = passwordEncoder.encode(newPassword);
        String confirmNewPassword = loginDataUpdateDTO.getNewPassword();
        String realPassword = account.get().getPassword();


        // Check if there is any new entered Password
        if (checkIfNotEmptyBlank(loginDataUpdateDTO.getCurrentPassword())){

            // Check if the entered Password matches the password in Database
            if (passwordEncoder.matches(enteredPassword,realPassword)
                    && checkIfNotEmptyBlank(newPassword)
                    && checkIfNotEmptyBlank(confirmNewPassword)){

                // Check the confirmation of new Password
                if (Objects.equals(newPassword,confirmNewPassword)){
                    account.get().setPassword(newPasswordHashed);
                }else {
                    return new ResponseEntity<>("New Password confirm not matches", HttpStatus.BAD_REQUEST);
                }
            }else {
                return new ResponseEntity<>("Wrong Password or empty new Password", HttpStatus.BAD_REQUEST);
            }
        }

        employeeRepository.save(account.get());
        return new ResponseEntity<>("Account has been updated", HttpStatus.OK);
    }


    public ResponseEntity<String> restPassword(RestPasswordDTO restPasswordDTO){
        Optional<Employee> account = employeeRepository.findById(restPasswordDTO.getAccountID());
        String mainKey = ClinicApplication.key;
        String enteredKey = restPasswordDTO.getKey();
        String newPassword = restPasswordDTO.getNewPassword();
        String confirmNewPassword = restPasswordDTO.getNewPasswordConfirm();

        boolean checkData =
                checkIfMatches(mainKey,enteredKey)
                && checkIfNotEmptyBlank(newPassword,confirmNewPassword)
                && checkIfMatches(newPassword,confirmNewPassword);

        if (checkData){
            account.get().setPassword(passwordEncoder.encode(newPassword));
            employeeRepository.save(account.get());
        }else {
            return  new ResponseEntity<>("Invalid input",HttpStatus.BAD_REQUEST);
        }

        return  new ResponseEntity<>("Password has been reseted",HttpStatus.OK);

    }

    private boolean checkIfUsernameIsTaken(String enteredUsername) {
        Optional<Employee> userToFindDB = employeeRepository.findUserByName(enteredUsername);
        return (userToFindDB.isPresent());
    }
    private boolean checkIfNotEmptyBlank(String text){
        return !text.isBlank() && !text.isEmpty();
    }

    private boolean checkIfNotEmptyBlank(String firstText, String secText){
        return !firstText.isBlank() && !firstText.isEmpty() && !secText.isEmpty() && !secText.isBlank();
    }
    private boolean checkIfMatches(String first, String sec){
        return Objects.equals(first,sec);
    }


}
