package com.managementSystem.clinic.DTOs;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MedicamentDTO {
    private int medicamentId;
    private String name;
    private int dose;
    private String producer;
}
