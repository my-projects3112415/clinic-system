package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.MedicamentByRecipeDTO;
import com.managementSystem.clinic.DTOs.PatientRecipesResponsesDTO;
import com.managementSystem.clinic.DTOs.RecipeRequestDTO;
import com.managementSystem.clinic.model.Medicament;
import com.managementSystem.clinic.model.Recipe;
import com.managementSystem.clinic.service.RecipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/L2")
public class RecipeController {

    private final RecipeService recipeService;



    // NOT IN USE
    @PutMapping("addRecipe")
    public ResponseEntity<String> addRecipeToPatient(@PathVariable int patientId, @RequestBody Recipe recipe){
       try {
           recipeService.addRecipeToPatient(patientId, recipe);
           return ResponseEntity.ok("Recipe has been added");
       }catch (Exception e){
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
       }

    }



    @GetMapping("getAllRecipesByPatientId/{patientId}")
    public ResponseEntity<List<PatientRecipesResponsesDTO>> getAllPatientRecipesById(@PathVariable int patientId){
        try {
            return ResponseEntity.ok(recipeService.getAllPatientRecipesById(patientId));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }


    @PutMapping("makeRecipeWithMedicament")
    public ResponseEntity<String> makeRecipeWithMedicament(@RequestBody RecipeRequestDTO recipeRequestDTO){
        try {
            recipeService.makeRecipeWithMedicament(recipeRequestDTO);
            return ResponseEntity.ok("Recipe has been created");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }

    }


    @GetMapping("getAllMedicamentByRecipe/{recipeID}")
    public ResponseEntity<List<MedicamentByRecipeDTO>> getAllMedicamentByRecipe(@PathVariable int recipeID){
        try {
            return ResponseEntity.ok(recipeService.getAllMedicamentByRecipe(recipeID));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }

    }


}
