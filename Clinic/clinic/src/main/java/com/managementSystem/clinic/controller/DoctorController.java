package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.DoctorResponseDTO;
import com.managementSystem.clinic.model.Doctor;
import com.managementSystem.clinic.service.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/")
public class DoctorController {
    private final DoctorService doctorService;


    @PostMapping("addNewDoctor")
    public ResponseEntity<String> addNewDoctor(@RequestBody Doctor doctor){
        try{
            doctorService.addDoctor(doctor);
            return ResponseEntity.ok("Doctor has been added");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PutMapping("editDoctor/{id}")
    public ResponseEntity<String> editDoctor(@PathVariable int id, @RequestBody Doctor doctor){
        try{
            doctorService.editDoctor(id,doctor);
            return ResponseEntity.ok("Doctor has been edited");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("getAllDoctors")
    public ResponseEntity<List<DoctorResponseDTO>> getAllDoctors(){
        try{
            return ResponseEntity.ok(doctorService.getAllDoctors());
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


}

