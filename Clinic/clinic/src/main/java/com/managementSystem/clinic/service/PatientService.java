package com.managementSystem.clinic.service;


import com.managementSystem.clinic.DTOs.PatientResponseSecDTO;
import com.managementSystem.clinic.DTOs.PatientResponseDTO;
import com.managementSystem.clinic.model.Patient;
import com.managementSystem.clinic.repository.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class PatientService {

    @Autowired
    private final PatientRepository patientRepository;


    public void addPatient(Patient patient) {
        patientRepository.save(patient);

    }


    public List<PatientResponseSecDTO> getPatientByLastname(String lastname){

        // Get the Patient list
        List<Patient> patientList = patientRepository.getPatientByLastname(lastname);

        // Create a new empty Patient list from a custom type
        List<PatientResponseSecDTO> patientResponseList = new ArrayList<>();

        patientList.forEach(patient -> {
            // Create empty Patient Object
            PatientResponseSecDTO newPatientDTO = new PatientResponseSecDTO();

            // Fill up the values
            newPatientDTO.setPatientId(patient.getPatientId());
            newPatientDTO.setFirstName(patient.getFirstName());
            newPatientDTO.setLastname(patient.getLastname());
            newPatientDTO.setAddress(patient.getAddress());
            newPatientDTO.setBirthday(patient.getBirthday());
            newPatientDTO.setTel(patient.getTel());
            newPatientDTO.setE_card(patient.getE_card());

            // Add the PatientDTO to our new Response List
            patientResponseList.add(newPatientDTO);
        });

        return patientResponseList;
    }


    public List<PatientResponseDTO> getAllPatient(){
        List<Patient> patients = patientRepository.findAll();
        List<PatientResponseDTO> responseDTOList = new ArrayList<>();

        patients.forEach( p -> {
            PatientResponseDTO patient = new PatientResponseDTO();
            patient.setPatientID(p.getPatientId());
            patient.setFirstName(p.getFirstName());
            patient.setLastname(p.getLastname());
            patient.setTel(p.getTel());
            patient.setAddress(p.getAddress());
            patient.setBirthday(p.getBirthday());
            patient.setE_card(p.getE_card());
            responseDTOList.add(patient);
        });
        return responseDTOList;
    }

    public PatientResponseSecDTO getPatientByE_Card(int e_card){
        PatientResponseSecDTO patientResponse = new PatientResponseSecDTO();
        Patient patientToFind = patientRepository.getPatientByE_Card(e_card);
        patientResponse.setPatientId(patientToFind.getPatientId());
        patientResponse.setFirstName(patientToFind.getFirstName());
        patientResponse.setLastname(patientToFind.getLastname());
        patientResponse.setTel(patientToFind.getTel());
        patientResponse.setAddress(patientToFind.getAddress());
        patientResponse.setBirthday(patientToFind.getBirthday());
        patientResponse.setE_card(patientToFind.getE_card());
        return patientResponse;
    }


    // NOT IN USE
    public Optional<Patient> getPatientById(int id) {
        return patientRepository.findById(id);
    }



    public void editPatient(int id, Patient patient) {
        Patient patientToEdit = patientRepository.getReferenceById(id);

        patientToEdit.setFirstName(patient.getFirstName());
        patientToEdit.setLastname(patient.getLastname());
        patientToEdit.setAddress(patient.getAddress());
        patientToEdit.setBirthday(patient.getBirthday());
        patientToEdit.setE_card(patient.getE_card());
        patientToEdit.setTel(patient.getTel());
        patientRepository.save(patientToEdit);
    }

    // NOT IN USE
    public void deletePatientById(int id) {
        patientRepository.deleteById(id);
    }





}
