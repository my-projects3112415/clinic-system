package com.managementSystem.clinic.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "appointmentId")

public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false)
    private int appointmentId;


    @ManyToOne
    @JsonBackReference("patientAppointment")
    @JoinColumn(name = "patient_Id")
    private Patient patient;


    @Column(nullable = false)
    private String time;


    @Column(nullable = false)
    private String date;

    public Appointment(String time, String date) {
        this.time = time;
        this.date = date;
    }

    public void setFromAppointment(Appointment source) {
        this.setDate(source.getDate());
        this.setTime(source.getTime());
    }
}
