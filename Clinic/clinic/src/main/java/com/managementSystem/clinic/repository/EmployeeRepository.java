package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {


    @Query(value = "SELECT * FROM Employee WHERE USER_NAME = :userName",nativeQuery = true)
    Optional<Employee> findUserByName(@Param("userName") String username);

}
