package com.managementSystem.clinic.repository;

import com.managementSystem.clinic.model.Timetable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimePlanRepository extends JpaRepository<Timetable,Integer> {
}
