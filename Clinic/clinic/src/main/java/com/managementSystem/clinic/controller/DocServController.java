package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.DocServicesDTO;
import com.managementSystem.clinic.DTOs.UpdateServiceDTO;
import com.managementSystem.clinic.model.DocService;
import com.managementSystem.clinic.service.DocServService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/")
public class DocServController {

//    @Autowired
    private final DocServService docServService;


    @GetMapping("L2/getAllServices")
    public ResponseEntity<List<DocServicesDTO>>  getAllServices(){
        try {
            return ResponseEntity.ok(docServService.getAllServices());
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PutMapping("addNewService")
    public ResponseEntity<String > addNewService(@RequestBody DocService docService){
        try {
            docServService.addNewService(docService);
            return  ResponseEntity.ok("Service has been added");
        }catch ( Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    @PutMapping("updateService")
    public ResponseEntity<String> updateService(@RequestBody UpdateServiceDTO updateServiceDTO){
        try {
            docServService.updateService(updateServiceDTO);
            return ResponseEntity.ok("Service has been updated");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
