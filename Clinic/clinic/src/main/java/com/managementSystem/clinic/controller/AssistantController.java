package com.managementSystem.clinic.controller;

import com.managementSystem.clinic.DTOs.AssistantResponseDTO;
import com.managementSystem.clinic.model.Assistant;
import com.managementSystem.clinic.service.AssistantService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("api/")
public class AssistantController {


    private final AssistantService assistantService;



    @PostMapping("L2/addAssistant")
    public ResponseEntity<String> addAssistant(@RequestBody Assistant assistant){
        try {
            assistantService.addAssistant(assistant);
            return ResponseEntity.ok("Assistant has been added");
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    // Not in Use
    @GetMapping("L2/L3/getAssistantBy/{id}")
    public ResponseEntity<Optional<Assistant>> getAssistantById(@PathVariable int id){
        try {
            return  ResponseEntity.ok(assistantService.getAssistantById(id));
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @PutMapping("L3/editAssistantBy/{id}")
    public ResponseEntity<String>  editAssistantById(@PathVariable int id,@RequestBody Assistant assistant){
        try {
            assistantService.editAssistantById(id, assistant);
            return ResponseEntity.ok("Assistant has been edited");
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }



    @DeleteMapping("L2/deleteAssistantBy/{id}")
    public ResponseEntity<String> deleteAssistant(@PathVariable int id){
        try {
            assistantService.deleteAssistantById(id);
            return ResponseEntity.ok("Assistant has been deleted");
        } catch (Exception e){
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("L2/getAllAssistants")
    public ResponseEntity<List<AssistantResponseDTO>>  getAllAssistants(){
        try {
            return  ResponseEntity.ok(assistantService.getAllAssistants());
        } catch (Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }











}
