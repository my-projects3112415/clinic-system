# Clinic System


## Projektbeschreibung:

Das Ziel des Projekts ist die Entwicklung einer Verwaltungsplattform für Arztpraxen. Die Plattform soll dazu dienen, Mitarbeiterkonten zu verwalten, einschließlich Stundenplänen und individueller Funktionalitäten für jeden Mitarbeiter. Des Weiteren ermöglicht sie die Verwaltung von Patienten, die Buchung von Terminen, die Erstellung von Rezepten und die Abrechnung für erbrachte Dienstleistungen. Die Plattform implementiert eine differenzierte Zugriffssteuerung, um sicherzustellen, dass Mitarbeiter nur auf die für sie relevanten Bereiche zugreifen können.

## Technische Beschreibung:

Die Datenbank umfasst die Entitäten Arzt, Assistent und Sekretärin, die alle vom Mitarbeiter erben. Dieses Vererbungsprinzip eliminiert Redundanzen, indem jedem Mitarbeiter unabhängig von seiner Kategorie ein Stundenplan zugewiesen wird. Die Beziehung zwischen Mitarbeiter und Stundenplan ist One-to-Many, um die Archivierung alter Stundenpläne zu ermöglichen.

Jeder Mitarbeiter kann mehrere Rollen haben, die spezifische Berechtigungen definiert und den Zugriff auf bestimmte Funktionen ermöglicht. Da die Berechtigungen zwischen den Mitarbeitern variieren, wurde eine Many-to-Many-Beziehung in der Datenbank implementiert, sodass eine Entität mehrere Rollen haben kann.

Patienten können Termine entweder mit vollständigen Informationen oder nur mit ihrem Namen erhalten. Es ist möglich, die Informationen eines Patienten zu einem späteren Zeitpunkt zu ergänzen. Zudem können Patienten Rezepte erhalten, die aus mehreren Medikamenten bestehen. Die Beziehung zwischen Patient und Rezept ist One-to-Many, während das Rezept selbst eine Many-to-Many-Beziehung mit den enthaltenen Medikamenten hat.

Sowohl die Sekretärin als auch der Arzt können Rechnungen erstellen, die mehrere Dienstleistungen umfassen.

<div style="  display: flex;
  flex-wrap: wrap;
  background-color: DodgerBlue;">

<img src="./Photos/b-1.jpeg" width="900" height="600">

</div>
